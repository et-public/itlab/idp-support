# Shibboleth Support Package

* [Custom Account Lockout Key Strategy](#Lockout)
* [Custom Endpoint Comparator](#Endpoint)

## <a name="Lockout">Custom Account Lockout Key Strategy</a>

Shibboleth IdP v3.3 (and later) support IdP-controlled account lockout
when there have been too many invalid authentication attempts for a
single account. The default implementation has the correct controls -
we can control how many failed attempts trigger the lockout, and how
long the lockout lasts - and it can store the lockout state in our
backend database. However, the default implementation uses the
`UsernameIPLockoutKeyStrategy` class to determine the key for the
database entry. `UsernameIPLockoutKeyStrategy` - as the name implies -
returns the combined username and client IP address as the key, which
only prevents multiple attempts from a single IP address. The
`UsernameLockoutKeyStrategy` implemented in this package returns just
the username as the key, so the lockout is triggered after 5 failed
logins for a single account from any IP address.

### Enabling UsernameLockoutKeyStrategy

The custom key strategy is enabled by adding two beans to `conf/authn/password-authn-config.xml`. First, define a new bean that references the `UsernameLockoutKeyStrategy` class from this package:

```xml
<bean id="AccountLockoutKeyStrategy" class="edu.stanford.itlab.shibboleth.UsernameLockoutKeyStrategy" />
```

Next, uncomment and edit the `shibboleth.authn.Password.AccountLockoutManager` bean, or create a new one if you have an older version of `password-authn-config.xml`:

```xml
<bean id="shibboleth.authn.Password.AccountLockoutManager"
      parent="shibboleth.StorageBackedAccountLockoutManager"
      p:lockoutKeyStrategy-ref="AccountLockoutKeyStrategy"
      p:maxAttempts="5"
      p:counterInterval="PT5M"
      p:lockoutDuration="PT5M"
      p:storageService-ref="shibboleth.JPAStorageService"
      p:extendLockoutDuration="false" />
```

If your `password-authn-config.xml` is based on an older Shibboleth install, add the `AccountLocked` event to that `AccountLocked` key in the `ClassifiedMessageMap` so that an appropriate message is displayed on the login form:

```xml
<util:map id="shibboleth.authn.Password.ClassifiedMessageMap">

  ...

  <entry key="AccountLocked">
    <list>
      <value>AccountLocked</value>
      <value>Clients credentials have been revoked</value>
      <value>KDC policy rejects request</value>
    </list>
  </entry>

  ...

</util:map>
```

## <a name="Endpoint">Custom Endpoint Comparator</a>

TL;DR: for [reasons](#Reasons), we need to have our IdP accept
requests to idp.stanford.edu on login.stanford.edu. The code in this
repo creates a JAR file that can be added to the IdP, and configured
to do what we need.

This is just a temporary fix until all the SPs are updated to use the
new login.stanford.edu endpoints.

### Code History

The code consists of two Java classes that are essentially copies of existing code. While `ReceviedEndpointSecurityHandler` could be implemented as a subclass of the OpenSAML class, there's little benefit since the longest function (`checkEndpointURI` has to be re-implemented to log warnings when the alias is used).

### edu.stanford.itlab.shibboleth.EndpointComparator

This is a copy of
[net.shibboleth.utilities.java.support.net.BasicURLComparator](http://grepcode.com/file/repo1.maven.org/maven2/net.shibboleth.utilities/java-support/7.1.1/net/shibboleth/utilities/java/support/net/BasicURLComparator.java),
modified to take `realHostname` and `aliasHostname` constructor
arguments. The main difference is that instances of `aliasHostname` in
each URI are replaced with `realHostname` before the URIs are
canonicalized.

### edu.stanford.itlab.shibboleth.ReceivedEndpointSecurityHandler

This is a copy of [org.opensaml.saml.common.binding.security.impl.ReceivedEndpointSecurityHandler](http://grepcode.com/file/repo1.maven.org/maven2/org.opensaml/opensaml-saml-impl/3.1.1/org/opensaml/saml/common/binding/security/impl/ReceivedEndpointSecurityHandler.java). The changes mainly support injectable `realHostname` and `aliasHostname` properties that can be passed to `EndpointComparator`; `aliasHostname` is also used to log warnings when requests are made to the old hostname.

### Download the JAR file

The [JAR](http://repo.itlab.stanford.edu/maven/release/edu/stanford/itlab/shibboleth/idp-support/1.0/idp-support-1.0.jar) and its GPG [signature](http://repo.itlab.stanford.edu/maven/release/edu/stanford/itlab/shibboleth/idp-support/1.0/idp-support-1.0.jar.asc) can be downloaded from the IT Lab [repo](http://repo.itlab.stanford.edu/).

Download the IT Lab Repo signing key:

```shell
% gpg --receive-keys 9A559486A1F8A1DA93809EA37CBDD8B10F39DA6C
gpg: key 7CBDD8B10F39DA6C: public key "Stanford IT Lab Repository <admin@itlab.stanford.edu>" imported
gpg: no ultimately trusted keys found
gpg: Total number processed: 1
gpg:               imported: 1
```

then verify the signature on the JAR file:

```shell
% gpg --verify idp-support-1.0.jar.asc idp-support-1.0.jar
gpg: Signature made Wed Mar 14 09:06:01 2018 PDT
gpg:                using RSA key 9A559486A1F8A1DA93809EA37CBDD8B10F39DA6C
gpg: Good signature from "Stanford IT Lab Repository <admin@itlab.stanford.edu>" [unknown]
...
```

### Build and Install the JAR file

Alternatively, you can clone this repo and build the JAR:

```shell
% mvn clean package
```

This will create `idp-support-1.0.jar` in the `target`
directory. Copy this file to the IdP (either under
`edit-webapp/WEB-INF/lib` if you're going to rebuild the WAR file, or
directly under `webapp/WEB-INF/lib` if you use an unpacked WAR).

### Configure the IdP

Configuration requires some changes to one of the system flows (icky, but temporary).

Look for the `ReceivedEndpointSecurityHandler` bean definition in `system/flows/saml/security-beans.xml`, and modify it:

```xml
<bean id="ReceivedEndpointSecurityHandler"
      class="net.shibboleth.idp.profile.impl.WebFlowMessageHandlerAdaptor"
      scope="prototype"
      c:executionDirection="INBOUND">
  <constructor-arg name="messageHandler">
    <bean class="edu.stanford.itlab.shibboleth.ReceivedEndpointSecurityHandler"
          scope="prototype"
          p:realHostname="://login"
          p:aliasHostname="://idp"
          p:httpServletRequest-ref="shibboleth.HttpServletRequest" />
  </constructor-arg>

  ...

</bean>
```

`p:realHostname` can be set to the real hostname
(_login.stanford.edu_ in our production environment), and
`p:aliasHostname` can be set to the alias (or old) hostname
(_idp.stanford.edu_ in our production environment).

If you want to re-use the same configuration in multiple environments
(for instance, if you're migrating _idp-dev.stanford.edu_,
_idp-test.stanford.edu_, and _idp.stanford.edu_ to
_login-dev.stanford.edu_, _login-test.stanford.edu_, and
_login.stanford.edu_, respectively), you can set `p:realHostname` and
`p:aliasHostname` to just the strings that need to be replaced (in
this case, `p:realHostname="://login"` and `p:aliasHostname="://idp"`.

### Configure the Alias Hostname on Tomcat

Create a virtual host in tomcat for the old server:

```xml
<Host name="idp.stanford.edu"
      unpackWARs="false"
      autoDeploy="false">
  <Context path="" docBase="/opt/idp_redirect">
    <Valve className="org.apache.catalina.valves.AccessLogValve"
           prefix="idp_redirect" suffix=".log"
           pattern='%h %l %u %t "%r" %s %b "%{Referer}i" "%{User-agent}i"' />
    <Valve className="org.apache.catalina.valves.RemoteAddrValve"
           addConnectorPort="true"
           allow="127\.0\.0\.1;80|::1;80|.*;80|.*;443"/>
    <Valve className="org.apache.catalina.valves.rewrite.RewriteValve" />
  </Context>
</Host>
```

Create `/opt/idp_redirect/META-INF/context.xml`, with this content:

```xml
<Context path="/" antiResourceLocking="false" />
```

Since a `web.xml` file is also required, create
`/opt/idp_redirect/WEB-INF/web.xml` with this content:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" version="3.0">

  <servlet-mapping>
    <servlet-name>default</servlet-name>
    <url-pattern>/*</url-pattern>
  </servlet-mapping>

</web-app>
```

Finally, create `/opt/idp_redirect/WEB-INF/rewrite.config`, with the following rules:

```conf
# All no-ECP POSTS are redirected with a 307 to login
# ECP POST request fall through to the final rule
RewriteCond %{REQUEST_METHOD} ^POST$
RewriteCond %{REQUEST_URI} !^/(idp/profile/SAML2/SOAP/ECP)$
RewriteCond %{HTTP_HOST} ^idp((-[^\.]+)?\.stanford\.edu)$
RewriteRule ^/(.*)$ https://login%1/$1 [R=307,L]

# rewrite metadata.xml to /idp/shibboleth,
# then pass through to login vhost
RewriteRule ^/metadata.xml$ /idp/shibboleth [C]
RewriteRule ^idp((-[^\.]+)?\.stanford\.edu)$ login$1 [H,L]

# metadata requests are handled by switching virtual hosts
RewriteCond %{REQUEST_URI} ^/idp/shibboleth/?$
RewriteRule ^idp((-[^\.]+)?.stanford.edu)$ login$1 [H,L]

# All remaining GETs on /idp/ paths are redirected to login
RewriteCond %{REQUEST_METHOD} ^GET$
RewriteCond %{HTTP_HOST} ^idp((-[^\.]+)?\.stanford\.edu)$
RewriteRule ^/(idp/.*)$ https://login%1/$1 [R=301]

# anything else is passed thru to the login vhost
RewriteRule ^idp((-[^\.]+)?\.stanford\.edu)$ login$1 [H,L]
```


### Logging

Warnings are logged when requests are recevied for the alias hostname:

    ... - WARN [...] - Message Handler:  Received request to https://weblogin.itlab.stanford.edu/idp/profile/SAML2/Redirect/SSO from https://webapp.itlab.stanford.edu/
    ... - WARN [...] - Message Handler:  Received request to https://weblogin.itlab.stanford.edu/idp/profile/SAML2/POST/SSO from https://webapp2.itlab.stanford.edu/


### <a name="Reasons">Reasons</a>

As part  of the migration  from our  legacy WebLogin WebSSO  system to
SAML 2.0,  we're also changing the  hostname of the page  where people
authenticate. Historically, people have  authenticated to a login form
on  weblogin.stanford.edu,  even  if  they were  using  our  SAML  IdP
(idp.stanford.edu). In order  to make the SAML IdP  name more generic,
we're "moving" it to login.stanford.edu.

Our IdP's SAML entity ID is not changing, but all the endpoints in the
metadata will  change from idp.stanford.edu to  login.stanford.edu, so
that the login page appears on login.stanford.edu.

Well-behaved SAML Service  Providers do not cause  any problems, since
they'll quickly pull the updated  metadata. Less well-behaved SPs will
cause  problems   -  they'll  still  have   the  old  idp.stanford.edu
endpoints,   so  people   would   be  prompted   to  authenticate   on
idp.stanford.edu.

Since the  SAML requests  contain the actual  endpoint, and  are often
signed  (and  therefore  cannot  be modified),  simple  redirects  are
insufficient.   Tucked deep  inside  the  IdP is  a  call  out to  the
OpenSAML libraries to check that  the requested endpoint URL (from the
SAML  request)  matches the  "expected"  endpoint  URL (based  on  the
hostname on which the request was received).


### Testing

I used [Browser Stack](https://browserstack.com/) to test the
redirects with a wide variety of OSes and browsers. The sites I was
using (https://webapp.itlab.stanford.edu/valid-user/ for HTTP-Redirect
testing, https://webapp2.itlab.stanford.edu/ for HTTP-POST testing,
https://weblogin.itlab.stanford.edu/ for the redirecting IdP, and
https://login.itlab.stanford.edu/ for the real IdP) were all
configured to require TLSv1.2 (production will support TLSv1.1).

With the exception of iOS 6, every browser that support TLSv1.2 also
supported both the 301 (HTTP-Redirect) and 307 (HTTP-POST) redirects.

### IdP Redirect Testing (TLSv1.2)

|OS|Browser|HTTP-Redirect|HTTP-POST|Notes|
|--|-------|-------------|---------|-----|
|Windows XP|IE 8|FAIL|FAIL|No TLSv1.2 Support|
|Windows 7|IE 8|OK|OK|Stanford Logo does not display|
|Windows 7|IE 9|OK|OK|-|
|Windows 7|IE 10|OK|OK|-|
|Windows 7|IE 11|OK|OK|-|
|Windows 7|Firefox 27|OK|OK|Redirect confirmation popup for HTTP-POST|
|Windows 7|Firefox 34|OK|OK|-|
|Windows 7|Chrome 30|OK|OK|-|
|Windows 8|IE 10 Metro|OK|OK|-|
|Windows 8|IE 10|OK|OK|-|
|Windows 8|Firefox 27|OK|OK|Redirect confirmation popup for HTTP-POST|
|Windows 8|Firefox 34|OK|OK|-|
|Windows 8|Chrome 30|OK|OK|-|
|Windows 8.1|IE 11 Metro|OK|OK|-|
|Windows 8.1|IE 11|OK|OK|-|
|Windows 10|IE 11|OK|OK|-|
|Windows 10|Edge 14|OK|OK|-|
|Windows 10|Edge 16|OK|OK|-|
|OS X Snow Leopard|Safari 5.1|FAIL|FAIL|No TLSv1.2 support|
|OS X Snow Leopard|Firefox 27|OK|OK|Redirect confirmation popup for HTTP-POST|
|OS X Snow Leopard|Firefox 34|OK|OK|-|
|OS X Snow Leopard|Chrome 30|OK|OK|-|
|OS X Lion|Safari 6|FAIL|FAIL|No TLSv1.2 support|
|OS X Lion|Firefox 27|OK|OK|Redirect confirmation popup for HTTP-POST|
|OS X Lion|Firefox 34|OK|OK|-|
|OS X Lion|Chrome 30|OK|OK|-|
|OS X Mountain Lion|Safari 6.2|FAIL|FAIL|No TLSv1.2 support|
|OS X Mountain Lion|Firefox 27|OK|OK|Redirect confirmation popup for HTTP-POST|
|OS X Mountain Lion|Firefox 34|OK|OK|-|
|OS X Mountain Lion|Chrome 30|OK|OK|-|
|OS X Mavericks|Safari 7.1|OK|OK|-|
|OS X Mavericks|Firefox 34|OK|OK|-|
|OS X Mavericks|Chrome 30|OK|OK|-|
|OS X Yosemite|Safari 8|OK|OK|-|
|Android 2.3 (Samsung Galaxy S2 emulator)|Android|FAIL|FAIL|No TLSv1.2 support|
|Android 4.1 (Samsung Galaxy S3 emulator)|Android|FAIL|FAIL|No TLSv1.2 support|
|Android 4.2 (Google Nexus 4 emulator)|Android|FAIL|FAIL|No TLSv1.2 support|
|Android 4.3 (Samsung Galaxy Note 3)|Chrome 58|OK|OK|-|
|Android 4.4 (Google Nexus 5)|Chrome 58|OK|OK|-|
|Android 4.4 (Samsung Galaxy S5)|Chrome 58|OK|OK|-|
|iOS 6 (iPhone 5 Simulator)|Safari|OK|FAIL|Stale request error on 307|
|iOS 7 (iPhone 5S)|Safari|OK|OK|-|
