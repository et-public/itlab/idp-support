package edu.stanford.itlab.shibboleth;

import javax.annotation.Nullable;

import com.google.common.base.Function;

import org.opensaml.profile.context.ProfileRequestContext;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.idp.authn.context.UsernamePasswordContext;

    
/**
 * A function to generate a key for lockout storage. This effectively defines
 * the scope of the lockout; e.g. if the key depends on the supplied username and
 * client IP address, the lockout will affect only attempts for that username
 * from that client IP.
 */

public class UsernameLockoutKeyStrategy implements Function<ProfileRequestContext,String> { 

  @Nullable public String apply(@Nullable final ProfileRequestContext profileRequestContext) {

    if (profileRequestContext == null) {
      return null;
    }

    final AuthenticationContext authenticationContext =
      profileRequestContext.getSubcontext(AuthenticationContext.class);

    if (authenticationContext == null) {
      return null;
    }

    final UsernamePasswordContext upContext =
      authenticationContext.getSubcontext(UsernamePasswordContext.class);

    if (upContext == null) {
      return null;
    }

    final String key = upContext.getUsername();
    if (key == null || key.isEmpty() ) {
      return null;
    }

    return key.toLowerCase();

  }

}

