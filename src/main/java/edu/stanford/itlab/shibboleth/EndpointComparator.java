package edu.stanford.itlab.shibboleth;

import java.net.MalformedURLException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.shibboleth.utilities.java.support.net.URIComparator;
import net.shibboleth.utilities.java.support.net.URIException;
import net.shibboleth.utilities.java.support.net.SimpleURLCanonicalizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EndpointComparator
 *
 */


public class EndpointComparator implements URIComparator {

  /** Logger. */
  @Nonnull private Logger log = LoggerFactory.getLogger(EndpointComparator.class);

  /** The real name for this IdP */
  private String aliasHostname;

  /** The alias name for this IdP */
  private String realHostname;

  /** The case-insensitivity flag. */
  private boolean caseInsensitive;


  public EndpointComparator(final String realName, final String aliasName) {
    super();
    realHostname = realName;
    aliasHostname = aliasName;
  }


  /**
   * Get the case-insensitivity flag value.
   * @return Returns the caseInsensitive.
   */
  public boolean isCaseInsensitive() {
    return caseInsensitive;
  }


  /**
   * Set the case-insensitivity flag value.
   * @param flag The caseInsensitive to set.
   */
  public void setCaseInsensitive(final boolean flag) {
    caseInsensitive = flag;
  }


  public boolean compare(@Nullable final String uri1, @Nullable final String uri2) throws URIException {

    if (uri1 == null) {
      return uri2 == null;
    } else if (uri2 == null) {
      return uri1 == null;
    } else {
      String uri1Canon = null;

      try {
        uri1Canon = SimpleURLCanonicalizer.canonicalize(uri1.replace(aliasHostname, realHostname));
      } catch (MalformedURLException e) {
        throw new URIException("URI was invalid: " + uri1Canon);
      }

      String uri2Canon = null;
      try {
        uri2Canon = SimpleURLCanonicalizer.canonicalize(uri2.replace(aliasHostname, realHostname));
      } catch (MalformedURLException e) {
        throw new URIException("URI was invalid: " + uri2Canon);
      }

      if (isCaseInsensitive()) {
        return uri1Canon.equalsIgnoreCase(uri2Canon);
      } else {
        return uri1Canon.equals(uri2Canon);
      }
    }
  }

}

